FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

# copy the patch from poky/meta/recipes-kernel/linux/files/
# to pass the perf building for visionfive2 board
SRC_URI:jh7110:append = "\
    file://0001-perf-cpumap-Make-counter-as-unsigned-ints.patch \
"
