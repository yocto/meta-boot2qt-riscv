DEPLOY_CONF_NAME:visionfive2 = "StarFive VisionFive 2"

IMAGE_FSTYPES:remove = " wic.gz"
IMAGE_FSTYPES += "wic.xz"
DEPLOY_CONF_IMAGE_TYPE = "wic.xz"

QBSP_IMAGE_CONTENT = "\
    ${IMAGE_LINK_NAME}.${DEPLOY_CONF_IMAGE_TYPE} \
    ${IMAGE_LINK_NAME}.conf \
    ${IMAGE_LINK_NAME}.info \
    "

# remove vulkan to dismiss depends on mesa and its conflicts with gles-user-module
DISTRO_FEATURES:remove = "vulkan"
